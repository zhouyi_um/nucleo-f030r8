# NUCLEO-F030R8

#### 介绍
基于NUCLEO-F030R8开发板的测试程序。

#### 硬件构架
使用NUCLEO-F030R8开发板。主芯片为 STM32F030R8t6，该板子 RAM 为 8KB，FLASH 为 64KB，主频最高为48MHz。

#### 软件架构
##### 开发工具
Win10 + IAR EARM 8.22.1 + Stm32CubeMX 6.3.0 + STM32F0 Package 1.11.3

目前已经测试以下部分：
##### 硬件 SPI2 连接 74HC595 控制 8 路继电器
在配置上使能硬件收发 SPI2，但是 74HC595 只收不发，所以 MISO 没有使用。 
74HC595侧        芯片侧
11脚 SHCP -> PB13    SPI2_SCK
12脚 STCP -> PB12    SPI2_NSS
13脚 OE   -> GND
14脚 DS1  -> PB15    SPI2_MOSI
如果使用软件模拟 SPI，代码也测试通过。
#### 软件模拟控制 74HC165
本来想硬件配置 SPI1 来控制 74HC165，但是时序配置一直不成功。只好暂时使用软件模拟时序控制。
74HC165侧       芯片侧
1 PL      -> PA4  SPI1_NSS    Output
2 CP  CLK -> PB3  SPI1_SCK    Output
9 QH  DS  -> PA6  SPI1_MISO   Output
#### 串口
使能 USART1。

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
